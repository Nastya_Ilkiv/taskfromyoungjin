# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 00:58:19 2016

@author: Nastya
"""

name = raw_input("Enter a file name: ")
try:
    f = open(name, "r")
except:
    print("There is no such file")
    exit()
else:
    f = open(name, "r")
    apacheResponse = {}
    for line in f:
        if line.startswith("75.98.230.254"):
            li = line.rstrip().split(" ")
            apacheResponse[li[8]] = apacheResponse.get(li[8], 0) + 1
    f.close()
    print "\n".join(["%s - %s" % (k,v) for k,v in apacheResponse.items()])