# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 01:28:36 2016

@author: Nastya
"""

name = raw_input("Enter a file name: ")
try:
    f = open(name, "r")
except:
    print("There is no such file")
    exit()
else: 
    f = open(name, "r")
    apache404 = {}
    for line in f:
        if "GET" in line:
            li = line.rstrip().split(" ")
            if li[6][-4:] == ".php" and li[8] == "404":
                apache404[li[0]] = apache404.get(li[0], 0) + 1
    f.close()
    print "\n".join(["%s - %s" % (k,v) for k,v in apache404.items()])