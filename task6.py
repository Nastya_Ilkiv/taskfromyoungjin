# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 01:47:39 2016

@author: Nastya
"""
    
name = raw_input("Enter a file name: ")
try:
    f = open(name, "r")
except:
    print("There is no such file")
    exit()
else:
    f = open(name, "r")
    URL = {}
    for line in f:
        if "GET" in line:
            li = line.rstrip().split(" ")
            if li[8] == "404":
                if li[6] in URL:
                    URL[li[6]][0] += 1
                    if li[0] not in URL[li[6]][1]:
                        URL[li[6]][1].append(li[0])
                else:
                    URL[li[6]] = [1, [li[0]]]
    li = []
    for key, val in URL.items():
        li.append((val, key))
    li.sort(reverse = True)
    f.close()
    print "\n".join(["%s - %s" % (v,k) for k,v in li[:10]])
    