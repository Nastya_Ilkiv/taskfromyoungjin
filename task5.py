# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 01:42:29 2016

@author: Nastya
"""
    
name = raw_input("Enter a file name: ")
try:
    f = open(name, "r")
except:
    print("There is no such file")
    exit()
else:
    f = open(name, "r")
    apache404 = {}
    for line in f:
        if "GET" in line:
            li = line.rstrip().split(" ")
            if li[8] == "404":
                apache404[li[0]] = apache404.get(li[0], 0) + 1
    li = []
    for key, val in apache404.items():
        li.append((val, key))
    li.sort(reverse = True)
    f.close()
    print "\n".join(["%s" % (v) for k,v in li[:10]])