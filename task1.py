# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 00:35:17 2016

@author: Nastya
"""
    
name = raw_input("Enter a file name: ")
try:
    f = open(name, "r")
except:
    print("There is no such file")
    exit()
else:
    count = 0
    for line in f:
        if line.startswith("75.98.230.254"):
            li = line.rstrip().split(" ")
            if li[8] == "404":
                count += 1
    f.close()
    print str(count), " attempts result in a 404 page response"